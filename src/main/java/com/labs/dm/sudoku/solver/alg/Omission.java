/*
 * Copyright Daniel Mroczka. All rights reserved.
 */

package com.labs.dm.sudoku.solver.alg;

import com.labs.dm.sudoku.solver.core.IMatrix;

/**
 * Omission algorithm implementation
 * <p/>
 * http://www.learn-sudoku.com/omission.html
 *
 * @author daniel
 */
public class Omission implements IAlgorithm {

    @Override
    public void execute(IMatrix matrix) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
